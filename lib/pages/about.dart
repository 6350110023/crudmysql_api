import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column();
  }
}

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('About'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Column(
        children: [
          image(
            assetImage: AssetImage("assets/6350110013.jpg"),
          ),
          SizedBox(height: 10),
          name(
            text: Text(
              "Panitan Kuedoy 6350110013  ICM",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 20),
          image(
            assetImage: AssetImage("assets/6350110023.jpg"),
          ),
          SizedBox(height: 10),
          name(
            text: Text(
              "Nattapat Chaiphat 6350110023  ICM",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold,),
            ),
          ),
        ],
      ),
    );
  }
}

class name extends StatelessWidget {
  const name({Key? key, required this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(25),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: Colors.black12,
        onPressed: () {},
        child: Row(
          children: [
            SizedBox(width: 3),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}

class image extends StatelessWidget {
  const image({Key? key, required this.assetImage}) : super(key: key);

  final AssetImage assetImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: SizedBox(
        height: 140,
        width: 140,
        child: CircleAvatar(
          backgroundImage: assetImage,
        ),
      ),
    );
  }
}